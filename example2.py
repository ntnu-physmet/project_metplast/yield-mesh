"""A module to showcase the features of yield_surface_mesh class.

the example files includes creating a mesh object, applying a proprtional load
using coordinates of nodes in the mesh object, refining mesh using different
refining strategies and finally plotting.
"""

import os
import numpy as np
from matplotlib import pyplot as plt
import yield_surface_mesh as ys
import abaqus_emulator as abq

# -----------------------------------------------------------------------------
# --------------------------- D I R E C T O R Y  ------------------------------
# Project Directory
PROJECT_DIR = "C:\\UMAT\\CPFEM_YS\\"
# Job Name
JOB_NAME = "YS_mises"
# ------------------------- I N I T I A T I O N  ------------------------------
# Creating a directory for the job
job_dir = f"{PROJECT_DIR}{JOB_NAME}\\".replace("\\", "/")
if not os.path.exists(job_dir):
    os.makedirs(job_dir)
# Creating a directory where the result txt files should be saved
res_dir = f"{job_dir}TXT_Outputs\\".replace("\\", "/")
if not os.path.exists(res_dir):
    os.makedirs(res_dir)
# ------------------------- P A R A M E T E R S  ------------------------------
with open(job_dir + "Run_params.txt", "r") as my_file:
    param_line = my_file.readlines()[35:]
pars = []
for i in range(len(param_line)):
    pars.append(list(map(str.strip, param_line[i].split(","))))
# line 1 from param file (initial)
model, n_total, n_equator = pars[0][0], int(pars[0][1]), int(pars[0][2])
# The next lines each hold information for one refining step
refining_modes, n_refining_steps, n_refining_batches = [], [], []
axises, zplanes, phis, ver_phis, autorefines = [], [], [], [], []
for par in pars[1:]:
    refining_modes.append(par[0])
    n_refining_steps.append(int(par[1]))
    n_refining_batches.append(int(par[2]))
    if refining_modes[-1] in ("band_by_plane", "band_by_angle", "vertex"):
        axises.append(np.array([float(par[3]), float(par[4]), float(par[5])]))
    else:
        axises.append(None)
    if refining_modes[-1] in ("band_by_plane"):
        zplanes.append(float(par[6]))
    else:
        zplanes.append(None)
    if refining_modes[-1] in ("band_by_angle"):
        phis.append(np.array([float(par[6]), float(par[7])]))
    else:
        phis.append(None)
    if refining_modes[-1] in ("vertex"):
        ver_phis.append(float(par[6]))
        autorefines.append(int(par[7]))
    else:
        ver_phis.append(None)
        autorefines.append(None)
#
# ------------------------------- B O D Y -------------------------------------
# Initializing mesh generation class
MyMesh = ys.YieldSurfaceMesh()
# -------------------------------> Step 1:
# ---------> Initial mesh:
# Generating the initial mesh structure
MyMesh.initial_sphere_mesh(n_total=n_total, n_equator=n_equator)
# -------------------------------> Step 2:
# ---------> Loading via ABAQUS:
print("--------------------------------------------------->>")
print("-------------------->> Analysing the initial nodes...")
print(f"Total number of loading cases: {MyMesh.n_nodes}")
abq.proportional_loading(MyMesh.nodes, res_dir)
# -------------------------------> Step 3:
# ---------> Initial yield surface:
# Deforming the initial mesh to obtain the yield surface by Updating the
# deformed node coordinates
# Interpolation parameter and level
INTERPOLATE_PARAM = "SDV_GAMMA"
INTERPOLATE_LVL = 2000e-6
MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
# initial plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area", savefig="surf0", hide=True)
#
# ------------------------ Refinemenet:
# reading the parameters
image_id = 1
for ri, refining_mode in enumerate(refining_modes):
    # refining_mode is the refinement strategy:
    # "area" or "curvature" or "band_by_plane" or "band_by_angle" or "vertex"
    # Number of refinement steps and max nodes to add in each refining step:
    n_refining_step = n_refining_steps[ri]
    n_refining_batch = n_refining_batches[ri]
    #
    if refining_mode in ("band_by_plane", "band_by_angle", "vertex"):
        # The axis of the refinement band
        axis = axises[ri]
    if refining_mode in ("band_by_plane"):
        # Defines where the axis of the band cuts the band plane
        zplane = zplanes[ri]
    if refining_mode in ("band_by_angle"):
        # Angles that define the band region, in range [0,2pi], where phi1<phi2
        phi1, phi2 = phis[ri][0], phis[ri][1]
    if refining_mode in ("vertex"):
        phi1, phi2 = 0, ver_phis[ri]
        # whether to perfrom autorefine
        autorefine = bool(autorefines[ri])
        if autorefine:
            normals, n_points = [], []
            normal = MyMesh.normal_in_direction(axis)
            normals.append(normal * np.sign(normal[0]))
            n_points.append(MyMesh.n_nodes)
    #
    for i in range(n_refining_step):
        if refining_mode in ("curvature", "area"):
            MyMesh.refine_mesh(
                method=refining_mode,
                batch_size=n_refining_batch,
                source_mesh="deformed",
            )
        if refining_mode in ("band_by_plane", "band_by_angle", "vertex"):
            if refining_mode in ("band_by_plane"):
                angle = MyMesh.find_angle_in_deformed_mesh(zplane)
                phi1, phi2 = angle - 1, angle + 1
            MyMesh.refine_region(
                axis=axis,
                phi1=phi1,
                phi2=phi2,
                batch_size=n_refining_batch,
                source_mesh="deformed",
            )
        # Loading via Abaqus
        print(f"---------------------------->> Refining step: {i+1}:")
        print(f"Total number of loading cases: {MyMesh.n_nodes}")
        abq.proportional_loading(MyMesh.nodes, res_dir)
        # creating the yield surface
        MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
        # saving the plot
        MyMesh.plot_mesh(mode='deformed', color_mode='area',
                         savefig='surf'+str(image_id), hide=True)
        image_id += 1
        if (refining_mode == "vertex" and autorefine):
            normal = MyMesh.normal_in_direction(axis)
            normals.append(normal * np.sign(normal[0]))
            n_points.append(MyMesh.n_nodes)

            # tightening the inspection angle
            n_newnodes = n_points[-1] - n_points[-2]
            if n_newnodes == n_refining_batch:
                phi2 = phi2 / 2
                print("new phi2:", phi2)

            # normal error calculation
            if i >= 1:
                normal_diff = np.abs(normals[-1] - normals[-2])
                print("max error:", max(normal_diff))
                # stop refining if difference is less than one percent
                if max(normal_diff) < 0.02:
                    break

# final 3D plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area")
#%%
# --------------------------- P L O T T I N G  --------------------------------
# Loading all results, original and interpolations
res_origs, res_interps = [], []
for i in range(MyMesh.n_nodes):
    # Whole original results:
    res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
    res_orig = ys.load_results(res_file, output_mode="original")
    res_origs.append(res_orig)
    # Interpolated levels only:
    levels = [10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6]
    res_interp = ys.load_results(
        res_file,
        output_mode="interpolate",
        interp_param="SDV_GAMMA",
        interp_range=levels,
    )
    res_interps.append(res_interp)


# What to plot?
PLOT_ISO_S12_SECTION = False
PLOT_INDIVIDUAL_PATH = False
PLOT_CLOUD = False
PLOT_AUTOREFINE = False

# plotting the  ISO-S12 section
if PLOT_ISO_S12_SECTION:
    # should be equal to the size of levels array used to generate res_interps
    which_lvls = [0, 0, 0, 0, 1]
    ys.plot_section(res_interps, MyMesh.elements, which_lvls, section_at=20, mode="2D")


# --------------> Cloud of points plot, of the whole loading pathes
if PLOT_CLOUD:
    # mode: 'all', 'levels', 'both'
    ys.plot_3d_cloud(res_origs, res_interps, mode="both")
#
# --------------> Plots for an individual loading path:
if PLOT_INDIVIDUAL_PATH:
    PATH_INDX = 8
    res_orig = res_origs[PATH_INDX]
    res_interp = res_interps[PATH_INDX]
    # ----> stress/strain and stress ratios/strain curve
    ys.plot_stress(res_orig, res_interp)

if PLOT_AUTOREFINE:
    normals, n_points = np.array(normals), np.array(n_points)
    fig1, ax1 = plt.subplots(figsize=(8, 6))
    for i in range(3):
        ax1.plot(n_points, normals[:, i], "o-")
    ax1.set_xlabel("number of nodes")
    ax1.set_ylabel("components of the normal vector")
    print(normals[-1, :])
