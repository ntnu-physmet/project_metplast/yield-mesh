"""A module to showcase the features of yield_surface_mesh class.

the example files includes creating a mesh object, applying a proprtional load
using coordinates of nodes in the mesh object, refining mesh using different
refining strategies and finally plotting.
"""

import os
import abaqus_emulator as abq
import yield_surface_mesh as ys

# -----------------------------------------------------------------------------
# --------------------------- D I R E C T O R Y  ------------------------------
# Project Directory
PROJECT_DIR = "C:\\UMAT\\CPFEM_YS\\"
# Job Name
JOB_NAME = "YS_EX"
# ------------------------- I N I T I A T I O N  ------------------------------
# Creating a directory for the job
job_dir = f"{PROJECT_DIR}{JOB_NAME}\\".replace("\\", "/")
if not os.path.exists(job_dir):
    os.makedirs(job_dir)
# Creating a directory where the result txt files should be saved
res_dir = f"{job_dir}TXT_Outputs\\".replace("\\", "/")
if not os.path.exists(res_dir):
    os.makedirs(res_dir)
# -----------------------------------------------------------------------------
#
# Initializing mesh generation class
MyMesh = ys.YieldSurfaceMesh()
#
# -------------------------------> Step 1:
# ---------> Initial mesh:
# Generating the initial mesh structure
MyMesh.initial_sphere_mesh(n_total=100, n_equator=0)
#
# -------------------------------> Step 2:
# ---------> Loading via ABAQUS:
print("--------------------------------------------------->>")
print(f"Total number of loading cases: {MyMesh.n_nodes}")
abq.proportional_loading(MyMesh.nodes, res_dir)
#
# -------------------------------> Step 3:
# ---------> Initial yield surface:
# Deforming the initial mesh to obtain the yield surface by Updating the
# deformed node coordinates
# Interpolation parameter and level
INTERPOLATE_PARAM = "SDV_GAMMA"
INTERPOLATE_LVL = 2000e-6
MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
#
# ------------------------ Refinemenet:
# Number of refinement steps and max nodes to add in each refining step
N_REFINING_STEPS = 4
N_REFINING_BATCH = 50
# Refinement strategy: "area" or "curvature" or "region"
REFINING_MODE = "area"
#
# The following params are required if REFINING_MODE="region" is chosen:
# The axis of the refinement band
axis = [-0.8, -1, 0.1]
# The angles that define the band, in range [0,2pi], where phi1<phi2
# Its possible to define angles so that the band includes a certain section
S12_SECTION = 30

# initial plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area", savefig="surf0", hide=True)

for i in range(N_REFINING_STEPS):
    if REFINING_MODE in ("curvature", "area"):
        MyMesh.refine_mesh(
            method=REFINING_MODE, batch_size=N_REFINING_BATCH, source_mesh="deformed"
        )
    if REFINING_MODE == "region":
        angle = MyMesh.find_angle_in_deformed_mesh(S12_SECTION)
        phi1, phi2 = angle - 1, angle + 1
        MyMesh.refine_region(
            axis=axis,
            phi1=phi1,
            phi2=phi2,
            batch_size=N_REFINING_BATCH,
            source_mesh="deformed",
        )
    # Loading via Abaqus
    print(f"---------------------------->> Refining step: {i+1}:")
    print(f"Total number of loading cases: {MyMesh.n_nodes}")
    abq.proportional_loading(MyMesh.nodes, res_dir)
    # creating the yield surface
    MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
    # saving the plot
    MyMesh.plot_mesh(
        mode="deformed", color_mode="area", savefig="surf" + str(i + 1), hide=True
    )

# final 3D plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area")
#%%
# --------------------------- P L O T T I N G  --------------------------------
# Loading all results, original and interpolations
res_origs, res_interps = [], []
for i in range(MyMesh.n_nodes):
    # Whole original results:
    res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
    res_orig = ys.load_results(res_file, output_mode="original")
    res_origs.append(res_orig)
    # Interpolated levels only:
    levels = [10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6]
    res_interp = ys.load_results(
        res_file,
        output_mode="interpolate",
        interp_param="SDV_GAMMA",
        interp_range=levels,
    )
    res_interps.append(res_interp)


# What to plot?
PLOT_ISO_S12_SECTION = False
PLOT_INDIVIDUAL_PATH = False
PLOT_CLOUD = False

# plotting the  ISO-S12 section
if PLOT_ISO_S12_SECTION:
    # should be equal to the size of levels array used to generate res_interps
    which_lvls = [0, 0, 0, 0, 1]
    ys.plot_section(
        res_interps, MyMesh.elements, which_lvls, section_at=S12_SECTION, mode="2D"
    )


# --------------> Cloud of points plot, of the whole loading pathes
if PLOT_CLOUD:
    # mode: 'all', 'levels', 'both'
    ys.plot_3d_cloud(res_origs, res_interps, mode="both")
#
# --------------> Plots for an individual loading path:
if PLOT_INDIVIDUAL_PATH:
    PATH_INDX = 8
    res_orig = res_origs[PATH_INDX]
    res_interp = res_interps[PATH_INDX]
    # ----> stress/strain and stress ratios/strain curve
    ys.plot_stress(res_orig, res_interp)
