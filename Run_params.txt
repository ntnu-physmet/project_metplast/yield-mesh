#1  ------------------------------- The first 35 lines will be skipped in reading--------------------------
#2  ====== model: 
#3         Directory name where the input files of the desired model are, under: 
#4         Project directory > Inp_files > model. Main input file is named: CPFEM_RVE.inp
#5  ====== n_total, n_equator:
#6         Total numger of initial nodes to generate, and of them, number of nodes on the equator.
#7  ====== refining_mode:
#8         Refinement strategy: "area" or "curvature" or "band_by_plane" or "band_by_angle" or "vertex"
#9  ====== n_refining_steps, n_refining_batch:
#10        Number of refinement steps and max nodes to add in each refining step
#11 ----------------------- The rest of parameters depends on the chosen refining mode: -------------------
#12 ====== area:      None 
#13 ====== curvature: None
#14 ====== band_by_plane:
#15 ============= axis1, axis2, axis3:
#16               The axis that defines the refinement band
#17 ============= zplane:
#18               Defines where the axis of the band cuts the band plane
#19 ====== band_by_angle:
#20 ============= axis1, axis2, axis3:
#21               The axis that defines the refinement band
#22 ============= phi1, phi2:
#23               The angles that define the band region, in range [0,2pi], where phi1<phi2 
#24 ====== vertex:
#25 ============= axis1, axis2, axis3:
#26               The axis that defines the vertex
#27 ============= phi2:
#28               The angles that defines the vertex region, in range [0 + epsilon ,2pi] 
#29 ============= autorefine:
#30               1 or 0. do we want to reduce the angle in each step if required?
#31               if so, phi2 will be the initial angle of vertex region.
#32 example:
#33
#34
#35 
    None,             20,        0
    area,              2,        20
    curvature,         2,        20
    band_by_plane,     2,        30,       0,   0,    1,    4.5
    band_by_angle,     2,        30,       0.8,   -1,    0.1,    20,  21
    vertex,            1,        30,       -0.8,   -1,    -0.1,    10,  0
    vertex,            100,       5,       -0.8,   -1,    0.1,    20,  1