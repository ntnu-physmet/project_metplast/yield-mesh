# CPFEM_Yield_Surface

*Manipulate yield surfaces as meshed geometries.*

![img](https://i.imgur.com/y0bxwGK.gif)

A python toolbox interfaced with abaqus to compute yield surface geometries on-the-fly.
This is part of the MetPlast project at [NTNU PhysMet](https://gitlab.com/ntnu-physmet)

## Installation 

```bash
pip install -r requirements.txt
```

*TODO*: provide `setup.py`

## Usage

See `example.py` to generate a first mesh, perform a refinement and plot the result.

