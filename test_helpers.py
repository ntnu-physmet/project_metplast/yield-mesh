"""Python unit testing

From this script's path:
>>> pytest -v # will process files that name starts with 'test_'
"""

import pytest
import numpy as np
import abaqus_emulator as ae


def test_abq_emulator_refstress():
    """The reference stress makes sense
    - runs for a float
    - runs for a 1darray of floats
    - crashes wih a string or other bad input
    """
    ae.voce_extended(1.0)
    ae.voce_extended(np.array(range(10)))
    for bad_input in ("string", {1,2,3}, list(range(10))):
        with pytest.raises(Exception) as excinfo:
            ae.voce_extended(bad_input)
        assert excinfo.errisinstance(TypeError)


def test_abq_emulator_mises():
    """The reference stress makes sense:
    - Runs for a 3x3 ndarray
    - Does not run for anything else
    """
    arr3 = np.array([1,2,3])
    arr33 = np.outer(arr3, arr3)
    ae.mises(arr33)
    for bad_input in (arr3, "string", {1,2,3}, list(range(10))):
        with pytest.raises(Exception) as excinfo:
            ae.mises(bad_input)
        assert excinfo.errisinstance(ValueError)


def test_abq_emulator_propload():
    """The proportonal loading function does the job.
    """
