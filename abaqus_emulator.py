"""A module for emulating abaqus.

A simple von-mises based solution with voce hardening is implemented to
showcase the yield_surface_mesh class features.
"""
import os
import numpy as np


def voce_extended(gamma):
    """Return  the reference stress of the eponymous function

    Parameters
    ----------
    gamma : float or list of floats

    Returns
    -------
    equivalent_stress : float or list of floats

    """
    # for 6061-T6
    tau0, tau1, theta0, theta1 = 112, 13, 230, 17
    return tau0 + (tau1 + theta1 * gamma) * (1 - np.exp(-gamma * theta0 / tau1))


def mises(stress):
    """Return the vonMises equivalent scalar"""
    hydrostatic = np.identity(3) * np.trace(stress) / 3
    deviatoric = stress - hydrostatic
    return np.sqrt(3 / 2) * np.linalg.norm(deviatoric)


def proportional_loading(nodes, res_dir):
    """Compute and store stress evolution with proportional loading.

    Parameters
    ----------
    nodes : pandas dataframe with columns named "x" , "y", "z" of type float
        each node defines a direction on which proportional loading is applied
        "x":S11 , "y":S22, "z":S12, the rest of stress components are assumed
        to be zero.
    res_dir : string
        the directory where the result txt files should be stored.


    """
    gamma = np.linspace(0, 2500e-6, 100)
    ref_stress = voce_extended(gamma)
    for i, node in nodes.iterrows():
        res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
        if os.path.exists(res_file):
            print(res_file, " exists!")
        else:
            # -----------------------------------------------------------------
            # building stress tensor
            n_mat = np.array(
                [
                    [node["x"], node["z"], 0.0],
                    [node["z"], node["y"], 0.0],
                    [0.0, 0.0, 0.0],
                ]
            )
            n_mat = n_mat / np.abs(n_mat[0, 0])
            n_mat = n_mat / np.linalg.norm(n_mat)
            k_steps = ref_stress / mises(n_mat)
            sig = np.zeros((len(gamma), 3, 3))
            for j, k_step in enumerate(k_steps):
                sig[j, :, :] = k_step * n_mat
            # ------------------------------------------------------------------
            # Print results to .txt file with the given job name
            to_write = np.column_stack(
                (
                    sig[:, 0, 0],
                    sig[:, 1, 1],
                    sig[:, 2, 2],
                    sig[:, 0, 1],
                    sig[:, 0, 2],
                    sig[:, 1, 2],
                    gamma,
                )
            )
            np.savetxt(
                res_file,
                to_write,
                delimiter=",",
                fmt="%.5e",
                header="S11, S22, S33, S12, S13, S23, SDV_GAMMA",
            )
